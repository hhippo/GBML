# Gaming Backup Multitool for Linux (GBML)

Gaming Backup Multitool for Linux (formerly known as Steam Linux Swiss Knife (SLSK)) is an open-source program that aims to automate
the backup and restore of Linux Steam games, their saves and configs, centralizing paths in a local and extremely lightweight database.

![screenshot](screenshot.png)

# Table of Contents

[Summary](#summary)

[Main features](#main-features)

[Statistics](#statistics)

[Installation](#installation)

* [Debian/RHEL-based distros](#debianrhel-based-distros)

* [Arch-based distros](#arch-based-distros)

* [Compiling from source](#compiling-from-source)

[How to use](#how-to-use)

[FAQ](#faq)

[How to contribute](#how-to-contribute)

[Special thanks](#special-thanks)

[Support](#support)

[License / Copyright](#license--copyright)

## Summary

There's little reliable information on the internet about save/config locations on Linux, and when it does show up, it's often "hit-or-miss".
Many players are getting tired of having to manually backup their games, which can be even more frustrating when there's too many of
them sitting at the library. Some of us just want to "set it and forget it". Problem is, while Windows users have GameSave Manager, there
is no alternative to Linux, and GSM is not going to be ported at all due to the fact it would need a separate database, since paths are
completely different between Windows/Mac/Linux.

This is why GBML was made: to provide a way to unify information about save/config paths of Linux Steam games into one open, lightweight
which everyone is invited to add this info to it as they see fit; and to use this database for automating backups of Linux Steam games,
saves and configurations. These goals will be easier to accomplish due to GBML's open-source nature: new releases come every day, and
considering it's pretty much inefficient for only one person to buy so many games solely for path documentation purposes, GBML provides
a solid base for anyone to contribute with this documentation. Plus, although most games follow (or at least should follow) the XDG Base
Directory Specifications for writing saves/configs (e.g. `~/.local/share`, `~/.config`, etc.), there is a good amount of games ignoring
this, writing saves and configs in other specific (and sometimes unknown and/or inadequate) locations of the system. Some of them may
not even have save files, or configurations may be stored exclusively via cloud. Regardless, it is up to us to find that out and
contribute so others can be helped.

## Main features

* **Store known paths for saves and configurations of Linux Steam games**
* **Automate the backup and restore of Linux Steam games, saves and configs with a fast GUI tool, using the Qt framework**

## Statistics

- *Total number of Linux games (entries) displayed on Steam at last update*: [**4629**](http://store.steampowered.com/search/?sort_by=Released_DESC&tags=-1&category1=998&os=linux)
- *Total number of registered games*: **4477 (96.72% from total)**
- *Number of registered games that have incomplete path information*: **3123 (69.76% from registered, 67.466% from total)**
- *Number of games temporarily not registered*: **88 (1.9% from total)**
- *Number of ignored entries*: **26 (0.56% from total)**
- *Most chronologically recent added game*: **Paper Dungeons Crawler (May 23, 2018)**

Check [CONTRIBUTING.md](CONTRIBUTING.md) and [MISSINGLIST.md](MISSINGLIST.md) for more information.

## Installation

**GBML is a 64-bit only program.** Before installing, make sure you have **Qt 5.5.0 or greater** installed (more details down below at [Compiling from source](#compiling-from-source)).

### Debian/RHEL-based distros

Since v1.4.0.0, GBML has deb and rpm binary packages available in the [tags](https://gitlab.com/supremesonicbrazil/GBML/tags) section.
Download the one for your system and run `dpkg -i` or `rpm -i` accordingly.

### Arch-based distros

GBML is available in the AUR as `gbml-git`. Install it using your AUR helper of choice.

### Compiling from source

1. Install the following dependencies:
  * **For Debian-based distros** - Debian/Ubuntu/Mint/etc. (`apt`):
    * Run dependencies: `qt5-default (>= 5.5.0)`
    * Build dependencies: `g++`, `make`, `qt5-qmake (>= 5.5.0)`, `sqlite3`
  * **For RHEL-based distros** - Fedora/OpenSUSE/etc. (`yum` or `dnf`):
    * Run dependencies: `qt5-devel (>= 5.5.0)`
    * Build dependencies: `gcc-c++`, `make`, `qt-qmake (>= 5.5.0)`, `sqlite-devel`
  * **For Arch-based distros** - Arch/Antergos/Manjaro/etc. (`pacman`):
	* Run and build: `qt5-base (>= 5.5.0)` - Qt5 and qmake
    * Build dependencies: `gcc`, `make`, `sqlite`
    
2. Download the repository, or clone it with `git clone https://gitlab.com/supremesonicbrazil/GBML`
3. Open a terminal inside the repo's root directory (or `cd` to it) and run `./BUILD.sh && sudo ./INSTALL.sh`
4. To uninstall GBML, run `sudo ./UNINSTALL.sh`

## How to use

1. Run `gbml` in a terminal, use the desktop file in `/usr/share/applications` or check your menu in the Utility section
2. Set your Steam library's `common` folder and the backup folder by clicking the `...` buttons (below the Start button)
3. Scan for content by clicking any of the scan buttons under "Backup Options" or "Restore Options", according to your preference
4. Mark which games you want to backup or restore in the list (you can mark or unmark all of them with the respective Mark All/Unmark All buttons)
5. Press the Start button to initiate the backup or restore process

## FAQ

* **Wait, wasn't this previously called Steam Linux Swiss Knife (SLSK)? Why the name change?**

Short answer: SLSK was conflicting with Soulseek (both had the same acronym) and "Swiss Army" is a trademark, so we had to change the name.

Long answer: see the entirety of issue #28

* **So it's basically a GameSave Manager for Linux?**

Yep, but it does a bit more. GSM only manages saves, GBML manages saves, configs *and* games, plus it's open-source.

* **Will you expand this to non-Steam games (GOG, Itch.io, repo games, etc.)? Will you do a version for Windows/Mac?**

For the time being, no, unless there's a big demand and lots of people willing to help.
Expanding to non-Steam games is definitely not impossible (just not the main focus right now), expanding to non-Linux platforms is another thing.
Even though I said before I "didn't want to compete with GSM and I'm not familiar with Mac" (and it's still a half-truth), I would be willing to
port it, *if and only if* there's a big chunk of people willing to help with this. I'd even rewrite the program as a whole in Python if people
needed it really bad, but doing it alone would require one big effort to create more databases and search for more paths, and I unfortunately
couldn't do this alone, so I'm really sorry.

* **Will I be VAC banned if I use this?**

GBML uses no login methods and no API whatsoever, it's all local file scanning so relax ;)

* **Can I use part of your program in a project of mine (code, database entries, etc.)? Why not build this into `insert-open-source-project-here`?**

This is more of a personal project that I want to share with people. Nothing stops you (or `insert-open-source-project-here`'s devs) from
incorporating parts of this project into whatever you like. Seriously, not only I give you full permission to use parts of this project, I also
strongly encourage you to do so. It's open-source after all. Everything from the code up to the database entries, use whatever your heart
desires to use. If you need help, get in touch so I can give you a hand to the best of my abilities :)

Just keep in mind two things: first, this project is under GNU GPL v3 - besides helping to avoid license incompatibilities, if you happen to
improve on whichever part you used, please contribute back, it'll mean a lot to us.

Second, [*please* **don't** do this](http://i0.kym-cdn.com/photos/images/original/001/079/173/ed2.png). Seriously, it's not cool and
people will hate you if they find out, please keep the original credits, even if it's just a tiny mention somewhere :(

* **But what about `insert-similar-project-name-here`? Y'know this guy's doing this and that, looks pretty much the same...**

*I know.* Even if there's a thousand alternatives popping up every second, the objective for each one is different, even if a bit slightly.
My objective with GBML is not to create another standard, but rather try to *adapt* to those standards.

Let's be honest - unless saves/configs are being written inside the own game's folder, which IMO would be the best of both worlds, every
dev making Linux games (especially those who came from Windows - not criticizing, just trying to raise attention) should at the very least be
following the [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html), that way
our lives would get easier and the whole paradigm of "Linux is too hard to use" would start to crack down like it has been since the past few
years. Until then, all we can do is keep on educating the following generations of new users, while giving them a little support so they can
walk on their own later, like we do with a toddler, and I believe this was one way I found to help with this re-education. Once people start
coming more towards Linux, among other things they *will* miss their GameSave Manager for sure (I was missing it myself, hence this
project was born), and having a native alternative that acts in a similar way (but probably improved) will weaken their resistance to change,
at least gaming-wise.

* **Why "Swiss Knife"/"Multitool"?**

Internal joke. That was the best way I found to describe what I wanted while in the planning phase - a project that has everything I need:
information, automation and ease of use, all in a single package, like a swiss knife - so I decided to put that nickname on it (before it got
renamed, that is). It became "Multitool" since I didn't want to lose this reference when renaming it.

Fun fact, I actually borrowed a swiss army knife from my dad so I could sketch the icons :)

## How to contribute

See [CONTRIBUTING.md](CONTRIBUTING.md) for more details.

## Special thanks

* [/r/linux_gaming](https://www.reddit.com/r/linux_gaming) - for being such an awesome and supportive community
* Everyone who contributed with issues, pull requests and suggestions - you guys rock ;)

## Support

Wanna buy me a beer? Donations are welcome:

* Bitcoin (BTC): 1MKwZku5DixVCuidtitSTYAJkykpHEhTLk
* Bitcoin Cash (BCH): qre8tju70m7a0c8xcgzxzhxv83avjcf2jca0hul9na
* Ethereum (ETH): 0xC1a41a4e9e04052AfC351DA5751283cA725454EF
* Litecoin (LTC): Li6swpBAi3fr6qYamNhWUnk4pJH2Q94YYZ

Using a currency that's not listed here? Please let me know! I'll be glad to add it here if possible :)

## License / Copyright

**Copyright (C) 2017-2018 Supremist (aka supremesonicbrazil)**

Reddit: [/u/TheSupremist](https://www.reddit.com/user/TheSupremist/)

Gaming Backup Multitool for Linux is available under the GNU GPL v3.0 license. See the accompanying [COPYING](COPYING) file for more details.
