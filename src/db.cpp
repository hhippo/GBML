/*
=========================================================================
---[GBML - Database functionalities]---
This file is part of Gaming Backup Multitool for Linux (or GBML for short).
Gaming Backup Multitool for Linux is available under the GNU GPL v3.0 license.
See the accompanying COPYING file for more details.
=========================================================================
*/

//-----------------------------------------------------------------------------------------------------------
// INCLUDED LIBRARIES
//-----------------------------------------------------------------------------------------------------------

// Standard C++/Qt libs
#include <cstddef>
#include <iostream>
#include <QDir>
#include <QDate>
#include <QTime>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QtDebug>
#include <stdio.h>
#include <stdlib.h>
#include <string>

// Program-specific/external libs
#include "db.h"
#include "mainmenu.h"
#include "progressmenu.h"
#include "sqlite3.h"

// DEBUG HACKS
//QTextStream& qStdOut()
//{
//    static QTextStream ts( stdout );
//    return ts;
//}

//-----------------------------------------------------------------------------------------------------------
// VARIABLES
//-----------------------------------------------------------------------------------------------------------

sqlite3 *conn;                                                  // Database connection handler
sqlite3_stmt *stmt;                                             // Database statement handler
QString UserFolder;                                             // Steam userdata folder (w/ user ID)
QDir DB::GBMLConfigPath = QDir::homePath() + "/.config/GBML";   // GBML config directory
std::string DB::DBPath = "/usr/share/gbml/SteamLinuxGames.db";  // Database raw path
std::string DB::SteamPath;                                      // Steamapps raw path
std::string DB::UserPath;                                       // Steam userdata raw path
std::string DB::UserID;                                         // Extracted from Steam userdata folder

//-----------------------------------------------------------------------------------------------------------
// FUNCTIONS
//-----------------------------------------------------------------------------------------------------------

// Checks where Steam is installed and sets paths accordingly (install path may vary depending on how it was installed, add additional paths here)
bool DB::CheckSteamInstall(){
    if (QDir(QDir::homePath() + QString::fromStdString("/.local/share/Steam")).exists()){   // Checks for "~/.local/share/Steam" (default path)
        UserFolder = QDir(QDir::homePath() + QString::fromStdString("/.local/share/Steam/userdata")).entryList(QDir::Dirs | QDir::NoDotAndDotDot).value(0);
        DB::UserID = QDir(UserFolder).dirName().toStdString();
        DB::UserPath = QDir::homePath().toStdString() + "/.local/share/Steam/userdata/" + UserFolder.toStdString();
        return true;
    } else if (QDir(QDir::homePath() + QString::fromStdString("/.steam/steam")).exists()){  // Checks for "~/.steam/steam" (path via Ubuntu's apt install)
        UserFolder = QDir(QDir::homePath() + QString::fromStdString("/.steam/steam/userdata")).entryList(QDir::Dirs | QDir::NoDotAndDotDot).value(0);
        DB::UserID = QDir(UserFolder).dirName().toStdString();
        DB::UserPath = QDir::homePath().toStdString() + "/.steam/steam/userdata/" + UserFolder.toStdString();
        return true;
    } else { return false; }                                                                // If Steam path is not found, tell main() to abort the program
}

// Fetches a game's individual info in the database, returns "NONE" if info doesn't exist
std::string DB::FetchGameInfo(std::string query){
    // Variables
    std::string result; // String for result

    sqlite3_open_v2(DB::DBPath.c_str(), &conn, SQLITE_OPEN_READONLY, NULL); // Opening database
    sqlite3_prepare_v2(conn, query.c_str(), -1, &stmt, 0);                  // Querying

    // Fetches the info if the row exists, if it doesn't then we put "NONE" inside the result variable
    if (sqlite3_step(stmt) == SQLITE_ROW){ result = reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0)); }
    else { result = "NONE"; }

    sqlite3_finalize(stmt); sqlite3_close_v2(conn); // Closing statement and database
    return result;                                  // Returning result
}

// Fetches a whole column from the database
QStringList DB::FetchColumn(std::string column){
    QStringList list; std::string query = "SELECT " + column + " FROM RegisteredGames";     // Setting list and query
    sqlite3_open_v2(DB::DBPath.c_str(), &conn, SQLITE_OPEN_READONLY, NULL);                 // Opening database
    sqlite3_prepare_v2(conn, query.c_str(), -1, &stmt, 0);                                  // Querying

    // Appending each entry individually in the result list
    while (sqlite3_step(stmt) == SQLITE_ROW){ list << reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0)); }

    sqlite3_finalize(stmt); sqlite3_close_v2(conn); // Closing statement and database
    return list;                                    // Returning list
}

// Replaces specific labels/aliases from the database - $STEAMAPPS, $STEAMUSER and ~ (home)
void DB::ReplaceLabels(std::string &string){

    // Checking if the path has ANY of the special labels - "[N/A]", "[UNKNOWN]" or "[CLOUD-ONLY]" - if yes, rename it to "SKIP" so the program ignores it
    if (string != "[N/A]" && string != "[UNKNOWN]" && string != "[CLOUD-ONLY]"){

        //qStdOut() << QString::fromStdString(ProgressMenu::CurrentPfx) << QString::fromStdString(",:,") << QString::fromStdString(string) << QString::fromStdString(",->,");
    
        // If "$STEAMAPPS" is found, replace that with the Steam path for that game
        if (string.find("$STEAMAPPS") == 0){ string.replace(0, 10, DB::SteamPath + "/" + ProgressMenu::CurrentFolder); }

        // If "$STEAMUSER" is found, replace that with a fixed path in Steam that represents the user's ID
        else if (string.find("$STEAMUSER") == 0){ string.replace(0, 10, DB::UserPath); }
        
        // If "$PFX" is found, replace that with the Steam path for that game
        else if (string.find("$STEAMPFXROOT") == 0){ string.replace(0, 13, ProgressMenu::CurrentPfx); }
        
        // If "$PFX_STEAM" is found, replace that with the Steam path for that game
        else if (string.find("$STEAMPFXSTEAM") == 0){ string.replace(0, 14, ProgressMenu::CurrentPfx + "/drive_c/Program Files (x86)/Steam"); }

        // If "$STEAMUSER" is found, replace that with a fixed path in Steam that represents the user's ID
        else if (string.find("$STEAMPFXUSER") == 0){ string.replace(0, 13, ProgressMenu::CurrentPfx + "/drive_c/Program Files (x86)/Steam/userdata/" + DB::UserID); }

        // If the shortcut for the home folder ("~") is found, replace that with the absolute home path
        else if (string.find("~") == 0){ string.replace(0, 1, QDir::homePath().toStdString()); }
            
        //qStdOut() << QString::fromStdString(string) << endl;
        
    } else {
        string = "SKIP";    // Replacing path with "SKIP"
    }
    
}

// Overload of ReplaceLabels for QStrings - see above
void DB::ReplaceLabels(QString &string){
    if (string != "[N/A]" && string != "[UNKNOWN]" && string != "[CLOUD-ONLY]"){
        //qStdOut() << QString::fromStdString(ProgressMenu::CurrentPfx) << QString::fromStdString(",:,") << string << QString::fromStdString(",->,");
        if (string.contains("$STEAMAPPS")){ string.replace("$STEAMAPPS", QString::fromStdString(DB::SteamPath + "/" + ProgressMenu::CurrentFolder)); }
        else if (string.contains("$STEAMUSER")){ string.replace("$STEAMUSER", QString::fromStdString(DB::UserPath)); }
        else if (string.contains("$STEAMPFXROOT")){ string.replace("$STEAMPFXROOT", QString::fromStdString(ProgressMenu::CurrentPfx)); }
        else if (string.contains("$STEAMPFXSTEAM")){ string.replace("$STEAMPFXSTEAM", QString::fromStdString(ProgressMenu::CurrentPfx + "/drive_c/Program Files (x86)/Steam")); }
        else if (string.contains("$STEAMPFXUSER")){ string.replace("$STEAMPFXUSER", QString::fromStdString(ProgressMenu::CurrentPfx + "/drive_c/Program Files (x86)/Steam/userdata/" + DB::UserID)); }
        else if (string.contains("~")){ string.replace("~", QDir::homePath()); }
        //qStdOut() << string << endl;
    } else {
        string = "SKIP";
    }
    
    
}
