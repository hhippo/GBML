 #!/bin/bash

# =========================================================================
# ---[GBML - Build Script]---
# This file is part of Gaming Backup Multitool for Linux (or GBML for short).
# Gaming Backup Multitool for Linux is available under the GNU GPL v3.0 license.
# See the accompanying COPYING file for more details.
# =========================================================================

# Importing aliases
shopt -s expand_aliases
source ~/.bash_aliases

# Compiling from source code in a temporary folder
mkdir bin

# Convert *.ods to *csv
libreoffice --headless --convert-to csv:"Text - txt - csv (StarCalc)":"44,34,76,2,,0,false,false,true" ./DB.ods

# Creating database
echo 'CREATE TABLE "RegisteredGames" (
`AppID`TEXT,
`SteamName`TEXT,
`GameFolder`TEXT,
`SavePath1`TEXT,
`SaveFolder1`TEXT,
`ConfigPath1`TEXT,
`ConfigFolder1`TEXT,
`SavePath2`TEXT,
`SaveFolder2`TEXT,
`ConfigPath2`TEXT,
`ConfigFolder2`TEXT,
`SavePath3`TEXT,
`SaveFolder3`TEXT,
`ConfigPath3`TEXT,
`ConfigFolder3`TEXT,
PRIMARY KEY(AppID)
);' | sqlite3 ./bin/SteamLinuxGames.db
echo ".import DB.csv RegisteredGames" | sqlite3 -separator ',' ./bin/SteamLinuxGames.db

