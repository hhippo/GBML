 #!/bin/bash

# =========================================================================
# ---[GBML - Build Script]---
# This file is part of Gaming Backup Multitool for Linux (or GBML for short).
# Gaming Backup Multitool for Linux is available under the GNU GPL v3.0 license.
# See the accompanying COPYING file for more details.
# =========================================================================

# Checking qmake installation
if [ -x /usr/bin/qmake-qt5 ]; then
    QMAKE=qmake-qt5     # Fedora
else
    QMAKE=qmake         # Other distros
fi

# Compiling from source code in a temporary folder
mkdir bin
$QMAKE -o ./bin/Makefile ./src/GamingBackupMultitoolLinux.pro
cd bin
make
mv GamingBackupMultitoolLinux gbml
cd ..

# Creating database
echo 'CREATE TABLE "RegisteredGames" (
`AppID`TEXT,
`SteamName`TEXT,
`GameFolder`TEXT,
`SavePath1`TEXT,
`SaveFolder1`TEXT,
`ConfigPath1`TEXT,
`ConfigFolder1`TEXT,
`SavePath2`TEXT,
`SaveFolder2`TEXT,
`ConfigPath2`TEXT,
`ConfigFolder2`TEXT,
`SavePath3`TEXT,
`SaveFolder3`TEXT,
`ConfigPath3`TEXT,
`ConfigFolder3`TEXT,
PRIMARY KEY(AppID)
);' | sqlite3 ./bin/SteamLinuxGames.db
echo ".import DB.csv RegisteredGames" | sqlite3 -separator ',' ./bin/SteamLinuxGames.db

# Creating desktop entry and setting it as executable
echo '[Desktop Entry]
Version=1.0
Name=Gaming Backup Multitool for Linux
Comment=Backup automation for Steam games, saves and configs
Exec=/usr/bin/gbml
Icon=/usr/share/gbml/img/icon.png
Terminal=false
Type=Application
Categories=Utility;Application;' > ./bin/gaming-backup-multitool-linux.desktop
chmod +x ./bin/gaming-backup-multitool-linux.desktop
