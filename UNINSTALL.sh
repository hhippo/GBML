#!/bin/bash

# =========================================================================
# ---[GBML - Uninstall Script]---
# This file is part of Gaming Backup Multitool for Linux (or GBML for short).
# Gaming Backup Multitool for Linux is available under the GNU GPL v3.0 license.
# See the accompanying COPYING file for more details.
# =========================================================================

# Checking if running as sudo
if [ $UID = 0 ]
then
    # Removing SLSK
    rm /usr/bin/gbml
    rm -r /usr/share/gbml
    rm /usr/share/applications/gaming-backup-multitool-linux.desktop
else
    echo "Please run this script as sudo."
    echo "Aborting..."
    echo "--------------------------------------------------------------------------------"
    exit
fi 
